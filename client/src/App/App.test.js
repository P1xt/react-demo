
import React from 'react';
import ReactDOM from 'react-dom';

import { shallow } from 'enzyme';
import { expect } from 'chai';

import App from './';
import Header from '../Header';

describe('<App />', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders the Header component', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.find(Header)).to.have.length(1);
  });
});
