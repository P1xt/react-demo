import React from 'react';
import { hot } from 'react-hot-loader';
import Header from '../Header';
import './App.scss';


const App = () => <div><Header /></div>;

export default hot(module)(App);
