import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

export default class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }
  render() {
    return (
      <div>
        <Navbar color="primary" dark expand="md">
          <NavbarBrand href="/">Speedrun2 - May 2018</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Frontend
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Tribute Page
                  </DropdownItem>
                  <DropdownItem>
                    Personal Portfolio Webpage
                  </DropdownItem>
                  <DropdownItem>
                    Random Quote Machine
                  </DropdownItem>
                  <DropdownItem>
                    Local Weather
                  </DropdownItem>
                  <DropdownItem>
                    Wikipedia Viewer
                  </DropdownItem>
                  <DropdownItem>
                    Twitch.tv
                  </DropdownItem>
                  <DropdownItem>
                    Calculator
                  </DropdownItem>
                  <DropdownItem>
                    Pomodoro Clock
                  </DropdownItem>
                  <DropdownItem>
                    Tic Tac Toe Game
                  </DropdownItem>
                  <DropdownItem>
                    Simon Game
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Data Visualization
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Markdown Previewer
                  </DropdownItem>
                  <DropdownItem>
                    Camper Leaderboard
                  </DropdownItem>
                  <DropdownItem>
                    Recipe Box
                  </DropdownItem>
                  <DropdownItem>
                    Game of Live
                  </DropdownItem>
                  <DropdownItem>
                    Roguelike Game
                  </DropdownItem>
                  <DropdownItem>
                    Bar Chart
                  </DropdownItem>
                  <DropdownItem>
                    Scatterplot Graph
                  </DropdownItem>
                  <DropdownItem>
                    Heat Map
                  </DropdownItem>
                  <DropdownItem>
                    Force Directed Graph
                  </DropdownItem>
                  <DropdownItem>
                    Data Across the Globe
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Backend
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Timestamp Microservice
                  </DropdownItem>
                  <DropdownItem>
                    Request Header Parser
                  </DropdownItem>
                  <DropdownItem>
                    URL Shortener
                  </DropdownItem>
                  <DropdownItem>
                    Image Search
                  </DropdownItem>
                  <DropdownItem>
                    File Metadata
                  </DropdownItem>
                  <DropdownItem>
                    Voting App
                  </DropdownItem>
                  <DropdownItem>
                    Nightlife Coordination
                  </DropdownItem>
                  <DropdownItem>
                    Stock Market
                  </DropdownItem>
                  <DropdownItem>
                    Book Trading Club
                  </DropdownItem>
                  <DropdownItem>
                    Pinterest Clone
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
